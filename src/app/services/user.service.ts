import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';

import {User} from '../models/user.model';
import {Observable} from 'rxjs';

@Injectable()
export class UserService {
  user: User;
  list: User[];
  private userUrl = '/api';

  constructor(private http: HttpClient) {
  }

  public getUsers(): Observable<User[]> {
    return this.http.get<User[]>(this.userUrl);
  }

  createUser(formData: User): Observable<any> {
    return this.http.post(this.userUrl, formData);

  }

  updateUser(formData: User): Observable<any> {
    return this.http.put(this.userUrl + '/' + formData.id, formData);

  }

  deleteUser(id: number): Observable<any> {
    return this.http.delete(this.userUrl + '/' + id);
  }

  refreshList(): void {
    this.getUsers().subscribe(data => {
      this.list = data;
    });
  }

  getUserById(id: number): Observable<User> {
    return this.http.get<User>(this.userUrl + '/' + id);
  }
}
