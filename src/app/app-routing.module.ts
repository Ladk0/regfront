import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {UserListComponent} from './users/user-list/user-list.component';
import {UserComponent} from './users/user/user.component';
import {UsersComponent} from './users/users.component';

const routes: Routes = [
  {path: 'users', component: UserListComponent},
  {path: 'signup', component: UserComponent},
  {path: '', component: UsersComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
