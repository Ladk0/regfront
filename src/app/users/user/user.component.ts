import {Component, Input, OnInit} from '@angular/core';
import {UserService} from '../../services/user.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {User} from '../../models/user.model';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})

export class UserComponent implements OnInit {
  @Input()
  user: User;
  reactForm: FormGroup;

  constructor(private userService: UserService, private formBuilder: FormBuilder, private router: Router) {
  }

  ngOnInit(): void {
    this.initForm();
  }

  initForm(): void {
    this.reactForm = this.formBuilder.group({
      id: [this.userService.user.id],
      firstName: [this.userService.user.firstName, [
        Validators.required,
        Validators.pattern(/[A-z]/)
      ]],
      lastName: [this.userService.user.lastName, [
        Validators.required,
        Validators.pattern(/[A-z]/)
      ]],
      email: [this.userService.user.email, [
        Validators.required,
        Validators.email
      ]],
      password: [this.userService.user.password, [
        Validators.minLength(8)
      ]]
    });
  }

  isControlInvalid(controlName: string): boolean {
    const control = this.reactForm.controls[controlName];
    return control.invalid && control.touched;
  }

  onSubmit(): void {
    const controls = this.reactForm.controls;

    if (this.reactForm.invalid) {
      Object.keys(controls)
        .forEach(controlName => controls[controlName].markAsTouched());
      return;
    }

    if (this.reactForm.value.id == null) {
      this.insertRecord();
    } else {
      this.updateRecord();
    }
    this.router.navigate(['./users']);
  }

  insertRecord(): void {
    this.userService.createUser(this.reactForm.value).subscribe(res => {
      this.initForm();
      this.userService.refreshList();
    });
  }

  updateRecord(): void {
    this.userService.updateUser(this.reactForm.value).subscribe(res => {
      this.initForm();
      this.userService.refreshList();
    });

  }
}
