import {Component, OnInit, ViewChild} from '@angular/core';
import {User} from '../../models/user.model';
import {UserService} from '../../services/user.service';
import {UserComponent} from '../user/user.component';
import {Router} from '@angular/router';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {
  @ViewChild(UserComponent) userCmp: UserComponent;

  constructor(private userService: UserService, private router: Router) {
  }

  ngOnInit(): void {
    this.userService.refreshList();
  }

  populateForm(user: User): void {
    this.router.navigate(['./signup']);
    this.userService.user = Object.assign({}, user);
  }

  onDelete(id: number): void {
    if (confirm('Are you sure to delete this record?')) {
      this.userService.deleteUser(id).subscribe(res => {
        this.userService.refreshList();
      });
    }
  }
}
